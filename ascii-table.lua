-- compact/one-line form (note: all strings in '', so you can run it with: lua -e "..." )
s='   01234567 89abcdef\n' for y=0,0xf0,0x10 do s=s..('%02x '):format(y) for x=0,0xf do s=s..string.char(y+x):gsub('%c','.') if x==7 then s=s..' ' end end s=s..(' %02x\n'):format(y) end print(s)
 
-- expanded/pretty-printed form
s='   01234567 89abcdef\n'
for y=0,0xf0,0x10 do
  s=s..('%02x '):format(y)
  for x=0,0xf do
    s=s..string.char(y+x):gsub('%c','.')
    if x==7 then
      s=s..' '
    end
  end
  s=s..(' %02x\n'):format(y)
end
print(s)
 
-- slightly smaller, less functional, more obfuscated variant
s=(('.'):rep(0x7f-0x1f):gsub('().',function(x) return string.char(0x1f+x) end):gsub('()('..('.'):rep(0x8)..')', function(i,l) return ('0x%02x %s\n'):format(i+0x20-1,l) end))
print(s)